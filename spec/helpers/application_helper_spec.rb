require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  include ApplicationHelper

  describe 'full title' do
    let(:product) { create(:product) }

    it 'トップページのタイトルが正常に表示されること' do
      expect(full_title('')).to eq 'BIGBAG Store'
    end

    it 'nilの場合' do
      expect(full_title(nil)).to eq 'BIGBAG Store'
    end

    it '商品ページが正常に表示されること' do
      expect(full_title(product.name)).to eq "#{product.name} - BIGBAG Store"
    end
  end
end
