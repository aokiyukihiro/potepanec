require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe '#show' do
    let(:product) { create(:product) }

    before do
      get :show, params: { id: product.id }
    end

    it '正常なレスポンスを返すこと' do
      expect(response).to be_successful
    end

    it 'showページが表示されること' do
      expect(response).to render_template :show
    end

    it '@productが適切であること' do
      expect(assigns(:product)).to eq product
    end
  end
end
