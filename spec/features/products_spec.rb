require 'rails_helper'

RSpec.feature "products", type: :feature do
  given(:product) { create(:product) }

  feature '商品ページ' do
    background do
      visit potepan_product_path(product.id)
    end

    scenario '商品ページが正しく表示されること' do
      within('.singleProduct') do
        expect(page).to have_content product.name
        expect(page).to have_content product.price
        expect(page).to have_content product.description
      end
    end

    scenario 'HOMEリンクが動くこと' do
      find('.link_to_light_section').click
      expect(current_path).to eq potepan_path
    end
  end
end
